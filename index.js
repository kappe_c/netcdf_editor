/**
 * This file is automatically loaded asynchronously by RequireJS because it is set as the value of
 * the `data-main` attribute.
 * The code here is responsible to set the configuration of RequireJS and to load the application
 * by means of filling the dedicated div element that is inside the body of index.html.
 */
requirejs.config({ baseUrl: 'int', paths: { ext: '../ext' } });
require(['../App'], App => new App(document.getElementById('appDiv')));

define(['EditorDims', 'EditorGloAtts', 'EditorVars', 'templates',
        'ext/netcdf', 'ext/nu'],
        (EditorDims, EditorGloAtts, EditorVars, templates,
         netcdf, nu) =>
/**
 * An instance of this class makes up the application.
 * It is intended to be loaded from *index.html* via *index.js*.
 */
class App {

constructor(par)
{
  this._form = nu.form(par);
  this.makeFileIO();
  this._editorGloAtts = new EditorGloAtts(this._form);
  this._editorDims = new EditorDims(this._form);
  this._editorVars = new EditorVars(this._form, this._editorDims.names, this._editorDims.recDimIdx);
  this._editorDims.onDomPat = (names, idx) => this._editorVars.resetDomainPattern(names, idx);
  this._editorDims.onRecDim = (p, q) => this._editorVars.changeRecordDimension(p, q);
  this._templateSel.onchange();
}

//------------------------------------------------------------------------------------------------//

/** Create the form elements to dump or generate a netCDF file. */
makeFileIO()
{
  const box = nu.div(this._form);
  
  const templateLbl = nu.label(box, 'templateSel');
  nu.txt(templateLbl, 'template:');
  templateLbl.title = templates.title;
  const templateSel = this._templateSel = nu.select(box, 'templateSel');
  nu.option(templateSel, 'none');
  templates.names.forEach(name => nu.option(templateSel, name));
  templateSel.onchange = () =>
  {
    const val = templateSel.value;
    if(val === 'none')
      this.reset();
    else
      this.dump(templates.get(val));
  };
  templateSel.value = templates.names[0];
  
  nu.span(box, ' or ');
  
  const inpLbl = nu.label(box, 'fileInp');
  nu.txt(inpLbl, 'input:');
  inpLbl.title = 'Open a netCDF file for viewing or editing. The original file is never altered.';
  const fileInp = nu.inpFile(box, 'fileInp', () =>
  {
    const file = fileInp.files[0];
    const reader = new FileReader();
    reader.onload = () =>
    {
      const ncReader = new netcdf.Reader(reader.result, file.name);
      let ncFile;
      try { ncFile = ncReader.read(); }
      catch(e) {
        if(e instanceof netcdf.SyntaxError) {
          console.error(e.message);
          alert(e.message);
          return;
        }
        else throw e;
      }
      this.dump(ncFile);
    };
    reader.readAsArrayBuffer(file);
  });
  
  nu.txt(nu.label(box, 'fileName'), 'output:');
  nu.input(box, 'button', 'generate').onclick = () => this.generate();
  this._fileNameInp = nu.input(box, 'text', 'NetCDF Editor Output', 'fileName');
  this._fileNameInp.title = 'Name for the file offered to save. The extension ".nc" is added.';
}

//------------------------------------------------------------------------------------------------//

/** Empty the input form. */
reset()
{
  this._fileNameInp.value = 'NetCDF Editor Output';
  this._editorDims.reset();
  this._editorGloAtts.reset();
  this._editorVars.reset();
}


/** Reset the input form with the content of a netCDF file. */
dump(ncFile)
{
  this._fileNameInp.value = ncFile.name.slice(0, -3);
  this._editorDims.reset(ncFile.dims, ncFile.numRecords);
  this._editorGloAtts.reset(ncFile.atts);
  this._editorVars.reset(ncFile.vars, ncFile.dims.map(dim => dim.name));
}

//------------------------------------------------------------------------------------------------//

/** Generate a netCDF file from the input and offer to download it. */
generate()
{
  const fileName = this._fileNameInp.value + '.nc';
  const numRecs = this._editorDims.numRecords;
  const dims = this._editorDims.getNc();
  const atts = this._editorGloAtts.getNc();
  const vars = this._editorVars.getNc(dims, this._editorDims.recDimIdx);
  const file = new netcdf.File(fileName, numRecs, dims, atts, vars);
  
  const arrBuf = netcdf.Writer.write(file);
  const blob = new Blob([arrBuf], { type: 'application/x-netcdf' });
  const a = nu.a(this._form, URL.createObjectURL(blob), '_blank');
  a.style.display = 'none';
  a.download = fileName;
  a.click();
  this._form.removeChild(a);
}

});

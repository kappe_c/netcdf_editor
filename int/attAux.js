define(['ext/netcdf', 'ext/nu'], (netcdf, nu) => {

const module = {};

//------------------------------------------------------------------------------------------------//

module.makeSel = (par, type = 'f64') =>
{
  const sel = nu.select(par);
  nu.option(sel, 'f64');
  nu.option(sel, 'f32');
  nu.option(sel, 'i32');
  nu.option(sel, 'i16');
  nu.option(sel, 'i8');
  nu.option(sel, 'char');
  sel.value = type;
  sel.className = 'CelledSelect';
  return sel;
};

//------------------------------------------------------------------------------------------------//

module.typeVal2typeTag =
{
  f64 : netcdf.TypedData.str2typeTag.Float64,
  f32 : netcdf.TypedData.str2typeTag.Float32,
  i32 : netcdf.TypedData.str2typeTag.Int32,
  i16 : netcdf.TypedData.str2typeTag.Int16,
  i8  : netcdf.TypedData.str2typeTag.Int8,
  char: netcdf.TypedData.str2typeTag.Uint8,
};

module.typeStr2typeVal =
{
  Float64: 'f64',
  Float32: 'f32',
  Int32  : 'i32',
  Int16  : 'i16',
  Int8   : 'i8',
  Uint8  : 'char',
};

//------------------------------------------------------------------------------------------------//

module.makeAtt = (name, str = '', type = 'char') =>
{
  let typeTag = netcdf.TypedData.str2typeTag.Uint8;
  if(type === 'char')
    return netcdf.Attribute.fromString(name, str);
  
  const letter = type.slice(0, 1);
  const bits = type.slice(1);
  let nums = str.split(', ');
  
  if(letter === 'f')
  {
    typeTag = netcdf.TypedData.str2typeTag['Float' + bits];
    nums = nums.map(x => parseFloat(x));
  }
  else if(letter === 'i')
  {
    typeTag = netcdf.TypedData.str2typeTag['Int' + bits];
    nums = nums.map(x => parseInt(x));
  }
  else
    console.error('Unknown type', type);
  
  return new netcdf.Attribute(name, typeTag, nums);
};

//------------------------------------------------------------------------------------------------//

return module;

});

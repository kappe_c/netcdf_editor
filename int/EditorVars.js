define(['EditorTabular', 'attAux', 'ext/netcdf', 'ext/nu'], (EditorTabular, attAux, netcdf, nu) =>
/**
 * Editor for variables.
 */
class EditorVars extends EditorTabular {

constructor(par, dimNames, recDimIdx)
{
  super(par, 'Variables');
  this._att2type = {};
  this._rowIdx2datOrRecs = [];
  this._recordDim = dimNames[recDimIdx];
  nu.th(this._hdr, 'domain').title = '(Possibly empty) list of dimension names separated by ", " (comma and blank).';
  nu.th(this._hdr, 'type');
  nu.th(this._hdr, 'data').title = 'Click a cell (with the preview) to view and edit the data.';
  this.makeAddBtn();
  
//   this.resetDomainPattern(dimNames, recDimIdx);
  
  const btn = nu.input(this._box, 'button', '+');
  btn.className = 'CelledButton AddRemAttButton';
  btn.title = `Add a variable attribute. It need not be defined for every variable. Multiple numbers must be separated by ", " (comma and blank).`;
  btn.onclick = () => this.addAtt();
}

//------------------------------------------------------------------------------------------------//

add(name = 'TODO', domain = '', type = 'f32', att2str = {}, datOrRecs = [0])
{
  const row = nu.tr(this._tbl);
  this._rows.push(row);
  this._rowIdx2datOrRecs.push(datOrRecs);
  
  this.makeNameInp(row, name);
  const domInp = nu.input(nu.td(row), 'text', domain);
  domInp.className = 'CelledInput DomainInp';
  domInp.onchange = () => this.onDomainChange(row)
  attAux.makeSel(nu.td(row), type);
  this.makeDataCell(row);
  this.makeRemBtn(row).addEventListener('click', () =>
  {
    const i2dat = this._rowIdx2datOrRecs;
    i2dat.splice(this._rows.indexOf(row), 1);
  });
  
  const atts = this.attCells.map(th => th.children[0].value);
  for(let att of atts) {
    const val = att2str[att];
    nu.input(nu.td(row), 'text', val === undefined ? '' : val).className = 'CelledInput';
  }
}

//------------------------------------------------------------------------------------------------//

resetDomainPattern(dimNames, recDimIdx)
{
  this._recordDim = dimNames[recDimIdx];
  // It is theoretically okay to depend multiple times on the same dimension,
  // e.g. to have a square domain.
  const any = '(' + dimNames.join('|') + ')';
  if(recDimIdx !== undefined)
    dimNames = dimNames.filter((name, idx) => idx !== recDimIdx);
  const anyButRec = '(' + dimNames.join('|') + ')';
  const pattern = '^(' + any + '?)(, ' + anyButRec + ')*$';
  for(let domInp of this._rows.map(row => row.children[1].children[0]))
    domInp.pattern = pattern;
}

//------------------------------------------------------------------------------------------------//

onDomainChange(row)
{
  const idx = this._rows.indexOf(row)
  const wasRecordVar = this._rowIdx2datOrRecs[idx][0] instanceof Array
  if(this.checkIsRecordVar(row) === false) {
    if(wasRecordVar === true)
      this.recordsToData(idx)
  } else {
    if(wasRecordVar === false)
      this.dataToRecords(idx)
  }
}

//------------------------------------------------------------------------------------------------//

/// Split the data into records
dataToRecords(rowIdx, newNumRecs = 1)
{
  const data = this._rowIdx2datOrRecs[rowIdx]
  const records = new Array(newNumRecs)
  const nPerRec = data.length / newNumRecs
  for(let rec = 0; rec < newNumRecs; ++rec)
  {
    const record = records[rec] = new Array(nPerRec)
    const off = nPerRec * rec
    for(let i = 0; i < nPerRec; ++i)
      record[i] = data[off + i]
  }
  this._rowIdx2datOrRecs[rowIdx] = records
}


/// Merge all records into the first one
recordsToData(rowIdx)
{
  const records = this._rowIdx2datOrRecs[rowIdx]
  const data = records[0] instanceof Array ? records[0] : Array.from(records[0])
  const oldNumRecs = records.length
  const nPerRec = data.length
  for(let rec = 1; rec < oldNumRecs; ++rec)
    for(let val of records[rec])
      data.push(val)
  this._rowIdx2datOrRecs[rowIdx] = data
}

//------------------------------------------------------------------------------------------------//

changeRecordDimension(oldDim, newDim, newNumRecs)
{
  this._rows.forEach((row, idx) =>
  {
    const firstDim = row.children[1].children[0].value.split(', ')[0]
    if(firstDim === newDim) // possibly comparison against undefined
      this.dataToRecords(idx, newNumRecs)
    else if(firstDim === oldDim) // possibly comparison against undefined
      this.recordsToData(idx)
  })
}

//------------------------------------------------------------------------------------------------//

addAtt(name = 'TODO name', type = 'char')
{
  const existingType = this._att2type[name];
  if(existingType !== undefined)
  {
    if(existingType === type)
      return;
    // TODO Choose broader type, like `(f32, f64) => f64` or `(i16, i32) => i32`.
    // If the new type is the broader one, update `this._att2type` and the GUI.
    
    return;
  }
  
  this._att2type[name] = type;
  let nameOld = name, typeOld = type;
  const hdr = this._hdr;
  const cell = nu.th(hdr);
  
  const inp = nu.input(cell, 'text', name);
  inp.className = 'CelledInput VarAttName';
  // NOTE This is not exactly the specification. (code clone from `makeNameInp`)
  inp.pattern = '^\\w[^]*$';
  inp.focus();
  inp.select();
  inp.oninput = () =>
  {
    delete this._att2type[nameOld];
    const nameNew = inp.value;
    this._att2type[nameNew] = typeOld;
    nameOld = nameNew;
  };
  
  const sel = attAux.makeSel(cell, type);
  sel.onchange = () =>
  {
    const typeNew = sel.value;
    this._att2type[nameOld] = typeNew;
    typeOld = typeNew;
  };
  
  const btn = nu.input(cell, 'button', '-');
  btn.onclick = () =>
  {
    // Earlier attributes may be deleted before this one, changing the column index it has at
    // construction time, which is why we compute it within this function every time.
    const colIdx = Array.from(hdr.children).indexOf(cell);
    Array.from(this._tbl.children).forEach(row => row.removeChild(row.children[colIdx]));
    delete this._att2type[nameOld];
  };
  btn.className = 'CelledButton AddRemAttButton';
  
  this._rows.forEach(row => nu.input(nu.td(row), 'text', '').className = 'CelledInput');
}

//------------------------------------------------------------------------------------------------//

getNc(dims, recDimIdx)
{
  const attCells = this.attCells;
  const attNames = attCells.map(th => th.children[0].value);
  const attTypes = attCells.map(th => th.children[1].value);
  const numAtts = attCells.length;
  
  const n = this._rows.length;
  const vars = new Array(n);
  for(let i = 0; i < n; ++i)
  {
    const row = this._rows[i];
    const cells = row.children;
    const get = colIdx => cells[colIdx].children[0].value
    
    const name = get(0);
    const typeTag = attAux.typeVal2typeTag[get(2)];
    const dimIndices = get(1).split(', ').map(dName => dims.findIndex(dim => dim.name === dName));
    const nPerRec = dimIndices.map(i => dims[i].numValues || 1).reduce((acc, val) => acc * val, 1);
    const isRecordVar = this.checkIsRecordVar(row);
    
    const attStrs = Array.from(cells).slice(5).map(td => td.children[0].value);
    const atts = [];
    for(let i = 0; i < numAtts; ++i)
      if(attStrs[i] !== '')
        atts.push(attAux.makeAtt(attNames[i], attStrs[i], attTypes[i]));
    
    const datOrRecs = this._rowIdx2datOrRecs[i];
    vars[i] = new netcdf.Variable(name, typeTag, dimIndices, nPerRec, isRecordVar, atts, datOrRecs);
  }
  return vars;
}

//------------------------------------------------------------------------------------------------//

reset(vars, dimNames)
{
  this.clear();
  this.attCells.forEach(th => this._hdr.removeChild(th));
  this._att2type = {};
  this._rowIdx2datOrRecs = [];
  if(vars === undefined || vars.length === 0)
    return;
  
  const typeStr2typeVal = attAux.typeStr2typeVal;
  for(let vbl of vars)
  {
    const att2str = {};
    for(let att of vbl.atts) {
      this.addAtt(att.name, typeStr2typeVal[att.type]);
      att2str[att.name] = att.string || att.data.join(', ');
    }
    const domain = vbl.dimIndices.map(i => dimNames[i]).join(', ');
    this.add(vbl.name, domain, typeStr2typeVal[vbl.type], att2str, vbl.records || vbl.data);
  }
}

//------------------------------------------------------------------------------------------------//

get attCells()
{
  return Array.from(this._hdr.children).slice(5);
}

//------------------------------------------------------------------------------------------------//
// There are two approaches one could follow concerning the variable data.
// Either store the data and fill the input field with it whenever it is launched or always keep
// the input field in memory (style.display = none) and generate the data from that.
// We choose the first approach. I.a. because to faithfully (re)store the data in case it was loaded
// from a file (going to and from string (i.e. a decimal representation of numbers) is lossy most
// of the time for floating point data).
// Maybe the data handling code should go into a separate file.
//------------------------------------------------------------------------------------------------//

checkIsRecordVar(row)
{
  return row.children[1].children[0].value.includes(this._recordDim);
}

//------------------------------------------------------------------------------------------------//

makeDataCell(row)
{
  const cell = nu.td(row);
  const preview = nu.span(cell, this.makeDataPreview(row));
  preview.className = 'DataPreview';
  preview.title = 'Click this cell to view and edit the data.';
  const state = { isOpen: false };
  preview.onclick = () =>
  {
    if(state.isOpen === true) {
      console.log('The data editor is already open');
      return;
    }
    this.makeDataInp(cell, row, state);
  };
  return cell;
}

//------------------------------------------------------------------------------------------------//

makeDataPreview(row)
{
  const f = x => x.toPrecision(4);
  const datOrRecs = this._rowIdx2datOrRecs[this._rows.indexOf(row)];
  const n = datOrRecs.length;
  const beg = datOrRecs[0];
  const lst = datOrRecs[n - 1];
  if(this.checkIsRecordVar(row) === false)
  {
    if(n === 1) return f(beg);
    if(n === 2) return f(beg) + ','  + f(lst);
    return             f(beg) + '…' + f(lst);
  }
  else
  {
    if(n === 1 && beg.length === 1) return f(beg[0]);
    if(n === 1 && beg.length === 2) return f(beg[0]) + ','  + f(beg[1]);
    return                                 f(beg[0]) + '…' + f(lst[lst.length - 1]);
  }
}

//------------------------------------------------------------------------------------------------//

makeDataInp(cell, row, state)
{
  state.isOpen = true;
  let mayHaveChanged = false;
  const box = nu.div(cell);
  box.className = 'DataInpBox';
  
  const hdr = nu.div(box);
  nu.input(hdr, 'button', 'close').onclick = () =>
  {
    cell.removeChild(box);
    state.isOpen = false;
    if(mayHaveChanged === false)
      return;
    const datOrRecs = this._rowIdx2datOrRecs[this._rows.indexOf(row)];
    const type = row.children[2].children[0].value;
    if(this.checkIsRecordVar(row) === false) {
      this.updateData(datOrRecs, inp.value.split(/\s*\s(?=\S)/), type);
    } else {
      const nOld = datOrRecs.length;
      const i2str = inp.value.split(/,\s*/);
      const nNew = i2str.length;
      for(let i = 0; i < nNew; ++i)
        datOrRecs[i] = this.updateData(datOrRecs[i], i2str[i].split(/\s*\s(?=\S)/), type);
      datOrRecs.splice(nNew);
    }
    cell.children[0].textContent = this.makeDataPreview(row);
  };
  const name = row.children[0].children[0].value;
  const domain = row.children[1].children[0].value;
  nu.span(hdr, 'data for ' + name + '(' + domain + ')');
  
  const inp = nu.textarea(box, undefined, 1);
  inp.placeholder = 'Data separated by whitespace, records additionally with a comma. "char" data must be entered as UTF-8 codes.';
  const datOrRecs = this._rowIdx2datOrRecs[this._rows.indexOf(row)];
  if(this.checkIsRecordVar(row) === false)
    nu.txt(inp, datOrRecs.join(' '));
  else
    nu.txt(inp, datOrRecs.map(rec => rec.join(' ')).join(',\n'));
  inp.oninput = () => mayHaveChanged = true;
  inp.focus();
  
  return inp;
}

//------------------------------------------------------------------------------------------------//

updateData(i2oldDat, i2newStr, type)
{
  if(i2oldDat === undefined) // new record
    i2oldDat = []
  
  const nOld = i2oldDat.length
  const nNew = i2newStr.length
  
  if(i2oldDat instanceof Array === false)
  {
    const type2ctor = {
      'f64': Float64Array, 'f32': Float32Array,
      'i32':   Int32Array, 'i16':   Int16Array,
      'i8' :    Int8Array, 'char':  Uint8Array,
    }
    const Ctor = type2ctor[type]
    if(i2oldDat.constructor !== Ctor) {
      i2oldDat = new Ctor(nNew)
    } else if(nNew !== nOld) {
      // Create a bigger array and copy the old data
      const temp = new Ctor(nNew)
      temp.set(i2oldDat)
      i2oldDat = temp
    }
  }
  else
    i2oldDat.splice(nNew)
  // i2oldDat has now the right type and size
  
  // Completely rewrite integer data
  if(type.charAt(0) !== 'f')
  {
    for(let i = 0; i < nNew; ++i)
      i2oldDat[i] = parseInt(i2newStr[i])
    return i2oldDat
  }
  
  // Update float data if it has changed
  const eps = Math.pow(10, -9)
  const abs = Math.abs
  for(let i = 0; i < nOld; ++i) {
    const datNew = parseFloat(i2newStr[i])
    if(abs(datNew - i2oldDat[i]) > eps)
      i2oldDat[i] = datNew
  }
  // Write the new float data
  for(let i = nOld; i < nNew; ++i)
    i2oldDat[i] = parseFloat(i2newStr[i])
  
  return i2oldDat
}

});

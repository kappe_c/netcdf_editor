define(['ext/netcdf'], (netcdf) => {

const module = {};

module.names = ['rectilinear mesh', 'unstructured mesh', 'meshless'];

module.title = 'These three templates all store the same (artificial) temperature data. The spatial domain of "rectilinear mesh" is made up by rectangular cells implicitly defined by the lon- and lat-axes; "unstructured mesh" uses cells which may be positioned arbitrarily and consist of any number of vertices; "meshless" is an example of how a point cloud can be stored in a netCDF file. Notice that the value of the "cell_methods" attribute is "area: point" by default for so called *intensive* quantities; so an unstructured mesh with no cell boundaries specified may serve as a point cloud description as well – your choice should depend on the intended semantics.';

const makeAtt = netcdf.Attribute.fromString;

//------------------------------------------------------------------------------------------------//

module.get = name =>
{
  return module[name.split(' ').join('_')]();
};

//------------------------------------------------------------------------------------------------//

module.rectilinear_mesh = () =>
{
  if(module._rectilinear_mesh !== undefined)
    return module._rectilinear_mesh;
  
  const fileName = 'myScalarField2DTdt.recti.nc';
  const numRecords = 3;
  const dims = [
    new netcdf.Dimension('time', 0),
    new netcdf.Dimension('lat', 2),
    new netcdf.Dimension('lon', 4),
    new netcdf.Dimension('bnds', 2)
  ];
  const atts = module.commonGloAtts;
  
  const i2lat = new netcdf.Variable('lat', 6, [1], 2, false, [
    makeAtt('axis', 'Y'), makeAtt('units', 'degrees_north'),
    makeAtt('long_name', 'latitude'), makeAtt('standard_name', 'latitude'),
    makeAtt('bounds', 'lat_bnds')
  ], [-45, 45]);
  
  const j2lon = new netcdf.Variable('lon', 6, [2], 4, false, [
    makeAtt('axis', 'X'), makeAtt('units', 'degrees_east'),
    makeAtt('long_name', 'longitude'), makeAtt('standard_name', 'longitude'),
    makeAtt('bounds', 'lon_bnds')
  ], [0, 90, 180, 270]);
  
  const i2v2lat = new netcdf.Variable('lat_bnds', 6, [1, 3], 4, false, [], [-89.5,0, 0,89.5]);
  const j2v2lon = new netcdf.Variable('lon_bnds', 6, [2, 3], 8, false, [], [
    -45,45, 45,135, 135,225, 225,315]);
  
  const t2i2j2tas = new netcdf.Variable('tas', 5, [0, 1, 2], 8, true, [
    makeAtt('units', 'degC'),
    makeAtt('long_name', 'Near-Surface Air Temperature'),
    makeAtt('standard_name', 'air_temperature')
  ], [[0,1,2,3,4,5,6,7], [8,9,10,11,12,13,14,15], [16,17,18,19,20,21,22,23]]);
  
  const vars = [module.timeCoords, i2lat, j2lon, module.timeBounds, i2v2lat, j2v2lon, t2i2j2tas];
  
  return module._rectilinear_mesh = new netcdf.File(fileName, numRecords, dims, atts, vars);
};

//------------------------------------------------------------------------------------------------//

module.unstructured_mesh = () =>
{
  if(module._unstructured_mesh !== undefined)
    return module._unstructured_mesh;
  
  const fileName = 'myScalarField2DTdt.unstr.nc';
  const numRecords = 3;
  const dims = [
    new netcdf.Dimension('time', 0),
    new netcdf.Dimension('cell', 8),
    new netcdf.Dimension('bnds', 4)
  ];
  const atts = module.commonGloAtts;
  
  const c2lat = new netcdf.Variable('lat', 6, [1], 8, false, [
    makeAtt('axis', 'Y'), makeAtt('units', 'degrees_north'),
    makeAtt('long_name', 'latitude'), makeAtt('standard_name', 'latitude'),
    makeAtt('bounds', 'cell_bnds_lat')
  ], [-45, -45, -45, -45, 45, 45, 45, 45]);
  
  const c2lon = new netcdf.Variable('lon', 6, [1], 8, false, [
    makeAtt('axis', 'X'), makeAtt('units', 'degrees_east'),
    makeAtt('long_name', 'longitude'), makeAtt('standard_name', 'longitude'),
    makeAtt('bounds', 'cell_bnds_lon')
  ], [0, 90, 180, 270, 0, 90, 180, 270]);
  
  const c2v2lat = new netcdf.Variable('cell_bnds_lat', 6, [1, 2], 32, false, [], [
    -89.5, 0, 0, -89.5,  -89.5, 0, 0, -89.5,  -89.5, 0, 0, -89.5,  -89.5, 0, 0, -89.5,
    0, 89.5, 89.5, 0,  0, 89.5, 89.5, 0,  0, 89.5, 89.5, 0,  0, 89.5, 89.5, 0
  ]);
  
  const c2v2lon = new netcdf.Variable('cell_bnds_lon', 6, [1, 2], 32, false, [], [
    -89.5, 0, 0, -89.5,  -89.5, 0, 0, -89.5,  -89.5, 0, 0, -89.5,  -89.5, 0, 0, -89.5,
    0, 89.5, 89.5, 0,  0, 89.5, 89.5, 0,  0, 89.5, 89.5, 0,  0, 89.5, 89.5, 0
  ]);
  
  const t2c2tas = new netcdf.Variable('tas', 5, [0, 1], 8, true, [
    makeAtt('units', 'degC'),
    makeAtt('long_name', 'Near-Surface Air Temperature'),
    makeAtt('standard_name', 'air_temperature'),
    makeAtt('coordinates', 'lat lon')
  ], [[0,1,2,3,4,5,6,7], [8,9,10,11,12,13,14,15], [16,17,18,19,20,21,22,23]]);
  
  const vars = [module.timeCoords, c2lat, c2lon, module.timeBounds, c2v2lat, c2v2lon, t2c2tas];
  
  return module._unstructured_mesh = new netcdf.File(fileName, numRecords, dims, atts, vars);
};

//------------------------------------------------------------------------------------------------//

module.meshless = () =>
{
  if(module._meshless !== undefined)
    return module._meshless;
  
  const fileName = 'myScalarField2DTdt.meshless.nc';
  const numRecords = 3;
  const dims = [
    new netcdf.Dimension('time', 0),
    new netcdf.Dimension('station', 8),
  ];
  const atts = module.commonGloAtts;
  atts.push(makeAtt('featureType', 'timeSeries'));
  
  const c2lat = new netcdf.Variable('lat', 6, [1], 8, false, [
    makeAtt('axis', 'Y'), makeAtt('units', 'degrees_north'),
    makeAtt('long_name', 'latitude'), makeAtt('standard_name', 'latitude')
  ], [-45, -45, -45, -45, 45, 45, 45, 45]);
  
  const c2lon = new netcdf.Variable('lon', 6, [1], 8, false, [
    makeAtt('axis', 'X'), makeAtt('units', 'degrees_east'),
    makeAtt('long_name', 'longitude'), makeAtt('standard_name', 'longitude')
  ], [0, 90, 180, 270, 0, 90, 180, 270]);
  
  const t2c2tas = new netcdf.Variable('tas', 5, [0, 1], 8, true, [
    makeAtt('units', 'degC'),
    makeAtt('long_name', 'Near-Surface Air Temperature'),
    makeAtt('standard_name', 'air_temperature'),
    makeAtt('coordinates', 'lat lon')
  ], [[0,1,2,3,4,5,6,7], [8,9,10,11,12,13,14,15], [16,17,18,19,20,21,22,23]]);
  
  const vars = [module.timeCoords, c2lat, c2lon, module.timeBounds, t2c2tas];
  
  return module._meshless = new netcdf.File(fileName, numRecords, dims, atts, vars);
};

//------------------------------------------------------------------------------------------------//

module.commonGloAtts =
[
  makeAtt('Conventions', 'CF-1.7'),
  makeAtt('title', 'A succinct description of what is in the dataset.'),
  makeAtt('institution', 'Specifies where the original data was produced.'),
  makeAtt('source', 'The method of production of the original data. If it was model-generated, source should name the model and its version, as specifically as could be useful. If it is observational, source should characterize it (e.g., "surface observation" or "radiosonde").'),
  makeAtt('history', 'Provides an audit trail for modifications to the original data. Well-behaved generic netCDF filters will automatically append their name and the parameters with which they were invoked to the global history attribute of an input netCDF file. We recommend that each line begin with a timestamp indicating the date and time of day that the program was executed.'),
  makeAtt('references', 'Published or web-based references that describe the data or methods used to produce it.'),
  makeAtt('comment', 'Miscellaneous information about the data or methods used to produce it.')
];

//------------------------------------------------------------------------------------------------//

module.timeCoords = new netcdf.Variable('time', 6, [0], 3, true, [
  makeAtt('axis', 'T'), makeAtt('units', 'days since 2018-09-03 00:00:00'),
  makeAtt('long_name', 'time'), makeAtt('standard_name', 'time'),
  makeAtt('bounds', 'time_bnds')], [[0], [1], [2]]);

module.timeBounds = new netcdf.Variable('time_bnds', 6, [0], 3, true, [],
                                        [[-0.5, 0.5], [0.5, 1.5], [1.5, 2.5]]);

//------------------------------------------------------------------------------------------------//

return module;

});

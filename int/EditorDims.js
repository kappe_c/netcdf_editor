define(['EditorTabular', 'ext/netcdf', 'ext/nu'], (EditorTabular, netcdf, nu) =>
/**
 * Editor for dimensions.
 */
class EditorDims extends EditorTabular {

constructor(par)
{
  super(par, 'Dimensions')
  this._onDomPat = () => {}
  this._onRecDim = () => {}
  nu.th(this._hdr, '#values')
  nu.th(this._hdr, 'rec').title = 'Is this the record dimension? (There can only be one or none.)'
  this.makeAddBtn()
}

//------------------------------------------------------------------------------------------------//

add(name = 'TODO', numValues = 1, isRecordDim = false)
{
  const newRow = nu.tr(this._tbl)
  this._rows.push(newRow)
  // We need another closure here (and for the remove button) because the `this._onDomPat` object
  // may change later.
  this.makeNameInp(newRow, name).oninput = () => this._onDomPat()
  nu.inpNumber(nu.td(newRow), undefined, numValues, 1, 1, Math.pow(2, 32) - 1).
    className = 'CelledInput'
  const recBtn = nu.inpCheckbox(nu.td(newRow), undefined, isRecordDim)
  recBtn.onchange = () =>
  {
    let oldDim, newDim, newNumRecs
    if(recBtn.checked === false)
      oldDim = newRow.children[0].children[0].value
    else
    {
      newDim = newRow.children[0].children[0].value
      newNumRecs = parseInt(newRow.children[1].children[0].value)
      for(let oldRow of this._rows)
      {
        if(oldRow !== newRow && oldRow.children[2].children[0].checked === true) {
          oldRow.children[2].children[0].checked = false
          oldDim = oldRow.children[0].children[0].value
          break
        }
      }
    }
    this._onDomPat()
    this._onRecDim(oldDim, newDim, newNumRecs)
  }
  this.makeRemBtn(newRow).addEventListener('click', () => this._onDomPat())
  this._onDomPat()
}

//------------------------------------------------------------------------------------------------//

get names()
{
  return this._rows.map(row => row.children[0].children[0].value)
}

//------------------------------------------------------------------------------------------------//

get recDimIdx()
{
  return this._rows.findIndex(row => row.children[2].children[0].checked === true)
}

//------------------------------------------------------------------------------------------------//

/**
 * Set the function that is called whenever the pattern of allowed domains changes;
 * i.e. a dimension is added, removed or renamed. The function gets called with an array of the
 * dimension names and the record dimension index.
 */
set onDomPat(fn)
{
  this._onDomPat = () => fn(this.names, this.recDimIdx)
}

//------------------------------------------------------------------------------------------------//

/**
 * Set the function that is called whenever the record dimension changes;
 * The function gets called with the old and new dimension name (either one may be undefined but
 * they are never the same) and the new number of records.
 */
set onRecDim(fn)
{
  this._onRecDim = fn
}

//------------------------------------------------------------------------------------------------//

/// @return 0 if there is no record dimension.
get numRecords()
{
  const recDimIdx = this.recDimIdx
  if(recDimIdx === -1)
    return 0
  return parseInt(this._rows[recDimIdx].children[1].children[0].value)
}

//------------------------------------------------------------------------------------------------//

getNc()
{
  return this._rows.map(row =>
  {
    const kids = row.children
    const get = i => kids[i].children[0].value
    return new netcdf.Dimension(get(0), kids[2].children[0].checked ? 0 : parseInt(get(1)))
  })
}

//------------------------------------------------------------------------------------------------//

reset(dims, numRecords)
{
  this.clear()
  if(dims === undefined || dims.length === 0)
    return
  for(let dim of dims)
  {
    if(dim.numValues !== 0)
      this.add(dim.name, dim.numValues)
    else
      this.add(dim.name, numRecords, true)
  }
}

})

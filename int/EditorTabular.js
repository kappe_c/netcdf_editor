define(['ext/nu'], (nu) =>
/**
 * Base class for a tabular editor.
 */
class EditorTabular {

constructor(par, heading)
{
  const box = this._box = nu.div(par);
  nu.h3(box, heading);
  this._tbl = nu.table(box);
  this._hdr = nu.tr(this._tbl);
  nu.th(this._hdr, 'name');
  this._kind = heading.toLowerCase().slice(0, -1);
  this._rows = []; // excluding the header
}

//------------------------------------------------------------------------------------------------//

makeAddBtn()
{
  const cell = nu.th(this._hdr);
  cell.className = 'AddRemButtonCell';
  const btn = nu.input(cell, 'button', '+');
  btn.title = `Add a ${this._kind}.`;
  btn.className = 'CelledButton AddRemButton';
  btn.onclick = () => this.add();
}

//------------------------------------------------------------------------------------------------//

makeRemBtn(row)
{
  const cell = nu.td(row);
  cell.className = 'AddRemButtonCell';
  const btn = nu.input(cell, 'button', '-');
  btn.title = `Remove this ${this._kind}.`;
  btn.onclick = () =>
  {
    this._rows.splice(this._rows.indexOf(row), 1);
    this._tbl.removeChild(row);
  };
  btn.className = 'CelledButton AddRemButton';
  return btn;
}

//------------------------------------------------------------------------------------------------//

makeNameInp(row, name)
{
  const inp = nu.input(nu.td(row), 'text', name);
  inp.className = 'CelledInput NameInp';
  // NOTE This is not exactly the specification.
  inp.pattern = '^\\w[^]*$';
  inp.focus();
  inp.select();
  return inp;
}

//------------------------------------------------------------------------------------------------//

clear()
{
  this._rows.forEach(row => this._tbl.removeChild(row));
  this._rows = [];
}

});

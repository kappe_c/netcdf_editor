define(['EditorTabular', 'attAux', 'ext/netcdf', 'ext/nu'], (EditorTabular, attAux, netcdf, nu) =>
/**
 * Editor for global attributes.
 */
class EditorGloAtts extends EditorTabular {

constructor(par)
{
  super(par, 'Global Attributes');
  nu.th(this._hdr, 'value').title = 'Non-char data must be separated by ", " (comma and blank).';
  nu.th(this._hdr, 'type');
  this.makeAddBtn();
}

//------------------------------------------------------------------------------------------------//

add(name = 'TODO name', value = 'NOTE missing value', type = 'char')
{
  const row = nu.tr(this._tbl);
  this._rows.push(row);
  this.makeNameInp(row, name);
  const ta = nu.textarea(nu.td(row), undefined, 0, undefined, 42);
  ta.className = 'CelledInput GloAttInp';
  ta.textContent = value;
  attAux.makeSel(nu.td(row), type);
  this.makeRemBtn(row);
}

//------------------------------------------------------------------------------------------------//

getNc()
{
  return this._rows.map(row =>
  {
    const cols = row.children;
    return attAux.makeAtt(cols[0].children[0].value,
                          cols[1].children[0].value,
                          cols[2].children[0].value);
  });
}

//------------------------------------------------------------------------------------------------//

reset(atts)
{
  this.clear();
  if(atts === undefined || atts.length === 0)
    return;
  for(let att of atts)
  {
    this.add(att.name, att.string || att.data.join(', '), attAux.typeStr2typeVal[att.type]);
  }
}

});

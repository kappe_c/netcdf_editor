# NetCDF Editor

Generate a netCDF file by filling in the header information in tabular forms and giving the data as
text entries separated by whitespace (records by comma).

This app is based on [netcdfjs](https://bitbucket.org/kappe_c/netcdfjs) and therefore inherits its
limitations, most noteworthy its restriction to the netCDF classic format.

The source code is published under the GPLv3 license.
An [instance of the app](https://www-user.rhrk.uni-kl.de/~kappe/netcdf-editor) is currently hosted
by the TU Kaiserslautern.
